(function($) {
  /*****No javascript*****/
  $('body').removeClass('no-js').addClass('is-js');
  // Fixed header
  //========================================
  $(window).on('scroll', function() {
    if ($('#header').hasClass('disp-header')) {
      $('#header.disp-header').css({ 'left': -$(window).scrollLeft(), 'width': 'auto' });
    } else {
      $('#header').css({ 'left': '', 'width': '' });
      if ($(window).scrollTop() < $('#header').height()) {
        $('#header').css({ 'left': '', 'width': '' });
      }
    }
  });
  //========================================
  //▼ Accordion Slider bar
  //========================================
  var accordion_faq = new $.DEVFUNC.accordion({
    wrap: $('.question-accordion'),
    item: $('.question-item'),
    target: '.question-heading',
    contents: $('.question-content'),
    contentStr: '.question-content',
    hasClassSub: 'question-content',
    callback: function(){},
    stepCallback: function(){},
  });
  if (accordion_faq.active) accordion_faq.init();
  //========================================
  //▼ Smartphone menu settings
  //========================================
	
  //========================================
  //▼ Processing for each breakpoint
  //========================================
  // Processing for each breakpoint
  $.HANDLEBREAKPOINT = function(device) {
    $.DEVFUNC.elemMove($.GSET.MOVE_ELEM, device); //Move element
  }
  $.DEVFUNC.MATCHMEDIA();
  //==================================================
  //▼ Only processed once when loading the screen end
  //==================================================
  var timer = false;
  $(window).resize(function() {
    if (timer !== false) {
      clearTimeout(timer);
    }
    timer = setTimeout(function() {
      
    }, 100);
  });

})(jQuery);